@extends('layouts.admin')
@section('title')
    <title>Trang chu</title>
@endsection
@section('content')
    <div class="content-wrapper">
        @include('partials.content-header',['name'=>'Menus','key'=>'Edit'])

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('menus.update',['id'=>$menuFollowIdEdit->id])}}  " method="post">
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên Menus</label>
                                <input  class="form-control" name="name" value="{{$menuFollowIdEdit->name}}" placeholder=" Nhập tên menu">

                            </div>
                            <div class="form-group">
                                <label >Chọn Menus Cha</label>
                                <select class="form-control" name="parent_id">
                                    <option value="0">chọn menu cha</option>
                                    {!! $optionSelect !!}
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection







